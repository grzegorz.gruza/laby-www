var cancelled = document.getElementsByClassName("cancelled")[0];
cancelled.addEventListener("click", function () {
    console.log("kliknięcie");
    setTimeout(function () { cancelled.style.background = "red"; }, 100);
    setTimeout(function () { cancelled.style.background = "orange"; }, 200);
    setTimeout(function () { cancelled.style.background = "yellow"; }, 300);
    setTimeout(function () { cancelled.style.background = "green"; }, 400);
    setTimeout(function () { cancelled.style.background = "blue"; }, 500);
    setTimeout(function () { cancelled.style.background = "indygo"; }, 600);
    setTimeout(function () { cancelled.style.background = "violet"; }, 700);
    setTimeout(function () { cancelled.style.background = "#B32437"; }, 800);
});
var formDate = document.getElementById("date");
var formName = document.getElementById("name");
var formMail = document.getElementById("mail");
document.getElementsByClassName("reservation")[0].addEventListener("input", function () {
    if ((formName.value).split(" ").length < 2
        || ((formName.value).split(" ").length == 2 &&
            (formName.value)[(formName.value).length - 1] == " ")
        || formMail.value === ""
        || formDate.value === ""
        || new Date(formDate.value) <= new Date()) {
        document.getElementById("submit").disabled = true;
    }
    else {
        document.getElementById("submit").disabled = false;
    }
    if ((formName.value).split(" ").length < 2
        || ((formName.value).split(" ").length == 2 &&
            (formName.value)[(formName.value).length - 1] == " ")) {
        formName.style.background = "#FFC3C3";
    }
    else {
        formName.style.background = "#C3FFB0";
    }
    if (formMail.value === "") {
        formMail.style.background = "#FFC3C3";
    }
    else {
        formMail.style.background = "#C3FFB0";
    }
    if (formDate.value === ""
        || new Date(formDate.value) <= new Date()) {
        formDate.style.background = "#FFC3C3";
    }
    else {
        formDate.style.background = "#C3FFB0";
    }
});
